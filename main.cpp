#include <QCoreApplication>
#include <iostream>
#include <vector>
using namespace std;
void initLists();
void HC();
void runHC();

void runGenetic();

struct material{
    string name;
    vector<material> dangList;
};

void genetic(vector<vector<pair<material , int> > > population);
material A,B,C,D,E;

int iterateCount = 0;

vector<vector<material> > answer;
vector<material> materials;


void showState(vector<vector<material> > state);
vector<vector<material> > succesor(vector<vector<material> > curState);

vector < vector<vector<material> > > allLocalWithSingleMat(vector<vector<material> > state , material tmp);

double evalState(vector<vector<material> > state);
double evalGen(vector<pair<material , int> > state);

bool isInDangList(material a , material b);
bool isLocalMin(vector<vector<material> > pre_state , vector<vector<material> > nxt_state);

bool isSameGen(vector<pair<material , int> > a , vector<pair<material , int> > b);


vector<pair<material , int> > fixStorage(vector<pair<material , int> > state);
vector<pair<material , int> > sortStorage(vector<pair<material , int> > state);

vector<pair<material , int > > mixGens(vector<pair<material , int> > a , vector<pair<material , int> > b);
bool shouldSurvive(vector<pair<material , int> > a);


int main()
{



    // baraye ejra shodan healClimbing dar tabe succesor - if moshakhas shode ra comment konid !
    initLists();


    answer.clear();

    materials.push_back(A);
    materials.push_back(B);
    materials.push_back(C);
    materials.push_back(D);
    materials.push_back(E);


    cout << "comment haye tabe main ra bekhanid !"<<endl << endl;
    cout << "jahate ejra shodane har algorithm un ro az comment kharej konid"<<endl << endl;


    // baraye ejra shodan healClimbing dar tabe succesor - if moshakhas shode ra comment konid !

    //healClimbing & simulated annealing

    runHC();

    //healClimbing & simulated annealing

    //genetic

    runGenetic();

    //genetic

}



void genetic(vector< vector<pair<material , int> > >population){

    int maxValue = -9999;
    bool isAnyGen = true;

    while(/*maxValue < - 6 && */isAnyGen){

        //cout << "!"<<endl;
        vector< vector<pair<material , int> > > tmpPopulation;
        tmpPopulation.clear();

        isAnyGen = false;
        maxValue = -9999;
        int count = 0;
        for(int i = 0 ; i < population.size() ; i++){

            int curVal = evalGen(population.at(i));

            if(curVal > maxValue){
                maxValue = curVal;
            }
            if(shouldSurvive(population.at(i)) == true){
                count++;
                tmpPopulation.push_back(population.at(i));
                if(count >= 2){
                    isAnyGen = true;
                }
            }

        }

        //   cout << "isAnyG : "<< isAnyGen<<endl;
        //   cout << "max value : "<<maxValue<<endl;

        population = tmpPopulation;
        tmpPopulation.clear();
        for(int i = 0 ; i < population.size() / 2 ; i++){
            vector<pair<material , int> > firstG = population.at(rand() % population.size());
            vector<pair<material , int> > secondG = population.at(rand() % population.size());
            vector<pair<material , int> > newG = mixGens(firstG , secondG);
            tmpPopulation.push_back(newG);
        }
        //     cout << "tmpPopSize : "<<tmpPopulation.size()<<endl;
        population = tmpPopulation;


        for(int p = 0 ; p < population.size() ; p++){
            for(int i = 0 ; i < population.at(p).size() ; i++){
                cout << population.at(p).at(i).second << " : "<<population.at(p).at(i).first.name<<endl;
            }
            cout << evalGen(population.at(p)) << endl;
            cout << "------------------------------------------"<<endl;
            cout << "------------------------------------------"<<endl;

        }
        cout << "new population !"<<endl;






    }
    //   cout << "END"<<endl;





}

vector<pair<material , int > > mixGens(vector<pair<material , int> > a , vector<pair<material , int> > b){
    int breakPoint = rand() % (materials.size() - 1);
    breakPoint++;
    vector<pair<material , int> > tmp;
    for(int i = 0 ; i < materials.size() ; i++){
        if(i < breakPoint){
            tmp.push_back(a.at(i));
        }
        else{
            tmp.push_back(b.at(i));
        }
    }
    return tmp;

}

bool shouldSurvive(vector<pair<material , int> > a){
    int value = evalGen(a);
    value += 35;
    int randTresh = rand()%30;
    if(randTresh > value){
        return false;
    }
    return true;
}

bool isSameGen(vector<pair<material , int> > a , vector<pair<material , int> > b){
    for(int i = 0 ; i < a.size() ; i++){
        if(a.at(i).first.name != b.at(i).first.name || a.at(i).second != b.at(i).second){
            return false;
        }
    }
    return true;
}

vector<pair<material , int> > sortStorage(vector<pair<material , int> > state){

    for(int s = 0 ; s < state.size() ; s++){

        for(int i = 0 ; i < state.size() - 1 ; i++){
            if(state.at(i).second > state.at(i+1).second){
                pair<material , int> tmp;
                tmp = state.at(i);
                state.at(i) = state.at(i+1);
                state.at(i+1) = tmp;
            }
        }

    }

    return state;

}

void runGenetic(){

    vector< vector<pair<material , int> > >population;
    population.clear();
    for(int p = 0 ; p < 20 ; p++){
        vector<pair<material , int> > tmpAns;
        tmpAns.clear();
        int stCount = (rand() % materials.size() ) + 1;
        for(int i = 0 ; i < materials.size() ; i++){
            pair<material , int> tmp;
            tmp.first = materials.at(i);
            tmp.second = rand()%stCount;
            tmpAns.push_back(tmp);
        }

        tmpAns = sortStorage(tmpAns);
        tmpAns = fixStorage(tmpAns);

        population.push_back(tmpAns);
    }

    //test
    //    for(int i = 0 ; i < population.at(0).size() ; i++){
    //        cout << population.at(0).at(i).second << " : "<<population.at(0).at(i).first.name<<endl;
    //    }
    //    cout << evalGen(population.at(0)) << endl;

    //    cout << population.size() << endl;
    genetic(population);
    //    cout << population.size() << endl;


    //    for(int i = 0 ; i < population.at(0).size() ; i++){
    //        cout << population.at(0).at(i).second << " : "<<population.at(0).at(i).first.name<<endl;
    //    }
    //    cout << evalGen(population.at(0)) << endl;


    return ;
    //test


}

double evalGen(vector<pair<material , int> > state){

    double eval = 0;

    for(int i = 0 ; i < state.size() ; i++){

        for(int j = 0 ; j < state.size() ; j++){

            if(state.at(i).second != state.at(j).second){
                continue;
            }

            if(isInDangList(state.at(i).first , state.at(j).first)){
                eval -= 3;
            }

        }

    }

    int max = 0;
    for(int i = 0 ; i < state.size() ; i++){

        if(state.at(i).second > max){
            max = state.at(i).second;
        }

    }
    eval -= max;
    eval -= 1;
    return eval;


}

vector<pair<material , int> > fixStorage(vector<pair<material , int> > state){
    int max = 0;
    for(int i = 0 ; i < state.size() ; i++){
        if(state.at(i).second > max){
            max = state.at(i).second;
        }
    }


    for(int i = 0 ; i < max ; i++){
        bool isIndexAvailable = false;
        for(int j = 0 ; j < state.size() ; j++){
            if(state.at(j).second == i){
                isIndexAvailable = true;
            }
        }

        if(!isIndexAvailable){
            for(int j = 0 ; j < state.size() ; j++){
                if(state.at(j).second > i){
                    state.at(j).second = state.at(j).second - 1;
                }
            }
            max--;
        }

    }
    return state;

}

void runHC(){
    vector<vector<material> > lastState;


    for(int i = 0 ; i < 10 ; i++){


        answer.clear();
        int stCount = rand()%materials.size()+1;
        for(int j = 0 ; j < stCount ; j++){
            vector<material> tmp;
            answer.push_back(tmp);
        }



        for(int j = 0 ; j < materials.size() ; j++){
            int stRand = rand()%answer.size();


            answer.at(stRand).push_back(materials.at(j));
        }


        lastState.clear();


        while(!isLocalMin(lastState , answer)){
            lastState = answer;
            showState(answer);
            HC();
            cout << " -------------------------------------- "<<endl;
        }
        cout << "END !"<<endl;

    }
}

void HC(){
    answer = succesor(answer);
}

vector<vector<material> > succesor(vector<vector<material> > curState){

    iterateCount ++;
    //cout << iterateCount << endl;
    vector<vector<vector<material> > > allStates;
    allStates.clear();

    vector<vector<material> > bestState;
    bestState.clear();

    for(int i = 0 ; i < curState.size() ; i++){

        for(int j = 0 ; j < curState.at(i).size() ; j++){

            material tmp = curState.at(i).at(j);
            vector<vector<material> > tmpAns = curState;
            tmpAns.at(i).erase(tmpAns.at(i).begin() + j);
            vector<vector<vector<material> > > curStates;
            curStates = allLocalWithSingleMat(tmpAns , tmp);
            for(int k = 0 ; k < curStates.size() ; k++){
                allStates.push_back(curStates.at(k));
            }
        }
    }


    for(int i = 0 ; i < allStates.size() ; i++){
        for(int j = 0 ; j < allStates.at(i).size() ; j++){
            if(allStates.at(i).at(j).size() == 0){
                allStates.at(i).erase(allStates.at(i).begin() + j);
            }
        }
    }



    //comment this if for healClimbing
    if(rand()%25 > iterateCount){
        bestState = allStates.at(rand() % allStates.size());
    }
    else{
        double maxValue = -9999;

        for(int i = 0 ; i < allStates.size() ; i++){
            double value = evalState(allStates.at(i));
            if(maxValue < value){
                maxValue = value;
                bestState = allStates.at(i);
            }
        }
    }


    return bestState;


}

double evalState(vector<vector<material> > state){

    double value = 0;

    for(int i = 0 ; i < state.size() ; i++){

        for(int j = 0 ; j < state.at(i).size() ; j++){


            for(int k = 0 ; k < state.at(i).size() ; k++){
                if(isInDangList(state.at(i).at(j) , state.at(i).at(k))){
                    value -= 3;
                }
            }



        }

    }
    return value - state.size();


}

void showState(vector<vector<material> > state){

    for(int i = 0 ; i < state.size() ; i++){
        for(int j = 0 ; j < state.at(i).size() ; j++){
            cout << state.at(i).at(j).name<<" | ";
        }
        cout<<endl;
    }

}

bool isInDangList(material a , material b){

    for(int i = 0 ; i < a.dangList.size() ; i++){
        if(a.dangList.at(i).name == b.name){
            return true;
        }
    }

    return false;

}

void initLists(){

    A.name="A";
    B.name="B";
    C.name="C";
    D.name="D";
    E.name="E";

    A.dangList.push_back(B);
    A.dangList.push_back(C);
    A.dangList.push_back(E);
    B.dangList.push_back(A);
    B.dangList.push_back(D);
    C.dangList.push_back(A);
    D.dangList.push_back(B);
    E.dangList.push_back(A);



}


vector < vector<vector<material> > > allLocalWithSingleMat(vector<vector<material> > state , material tmp){


    vector < vector<vector<material> > > allStates;
    allStates.clear();
    for(int i = 0 ; i < state.size() ; i++){

        for(int j = 0 ; j < state.at(i).size() ; j++){
            vector<vector <material> > tmpVec;
            tmpVec.clear();
            tmpVec = state;
            tmpVec.at(i).push_back(tmp);
            allStates.push_back(tmpVec);
        }

    }
    vector<material> newStorage;
    newStorage.clear();
    newStorage.push_back(tmp);
    state.push_back(newStorage);
    allStates.push_back(state);

    return allStates;


}

bool isLocalMin(vector<vector<material> > pre_state , vector<vector<material> > nxt_state){
    if(pre_state.size() != nxt_state.size()){
        return false;
    }
    for(int i = 0 ; i < pre_state.size() ; i++){
        if(pre_state.at(i).size() != nxt_state.at(i).size()){
            return false;
        }
    }

    return true;

}
















